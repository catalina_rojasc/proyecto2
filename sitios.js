
function SitiosInteres(){
    var mymap = L.map('visor',  { center:[4.625770, -74.172777],  zoom:16 });
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {        
            maxZoom: 20,
            id: 'mapbox.streets'
         }).addTo(mymap);

         var popup1 = L.popup()
         .setLatLng([4.649317, -74.061678])
         .setContent("Iglesia Nuestra Señora de Lourdes")
         .openOn(mymap);
        var marker = L.marker([4.649317, -74.061678]).addTo(mymap);
        marker.bindPopup("<b>Iglesia Nuestra Señora de Lourdes</b><br>").openPopup();
        var circle = L.circle([4.649317, -74.061678], {
            color: 'red',
            fillColor: '#f03',
            fillOpacity: 0.5,
            radius: 50
        }).addTo(mymap);

        var popup2 = L.popup()
         .setLatLng([4.673647, -74.044169])
         .setContent("Museo El Chicó")
         .openOn(mymap);
         var marker = L.marker([4.673647, -74.044169]).addTo(mymap);
         marker.bindPopup("<b>Museo El Chicó</b><br>").openPopup();
         var circle = L.circle([4.673647, -74.044169], {
            color: 'blue',
            fillColor: 'blue',
            fillOpacity: 0.5,
            radius: 50
        }).addTo(mymap);

         var popup3 = L.popup()
         .setLatLng([4.677057, -74.048525])
         .setContent("Parque de la 93")
         .openOn(mymap);
         var marker = L.marker([4.677057, -74.048525]).addTo(mymap);
         marker.bindPopup("<b>Parque de la 93</b><br>").openPopup();
         var circle = L.circle([4.677057, -74.048525], {
            color: 'green',
            fillColor: 'green',
            fillOpacity: 0.5,
            radius: 50
        }).addTo(mymap);

        var popup4 = L.popup()
        .setLatLng([4.674216, -74.056489])
        .setContent("Parque El Virrey")
        .openOn(mymap);
         var marker = L.marker([4.674216, -74.056489]).addTo(mymap);
         marker.bindPopup("<b>Parque El Virrey </b><br>").openPopup();
         var circle = L.circle([4.674216, -74.056489], {
            color: 'yellow',
            fillColor: 'yellow',
            fillOpacity: 0.5,
            radius: 50
        }).addTo(mymap);

        var popup5 = L.popup()
        .setLatLng([4.667709, -74.054040])
        .setContent("Zona T")
        .openOn(mymap);
         var marker = L.marker([4.667709, -74.054040]).addTo(mymap);
         marker.bindPopup("<b>Zona T </b><br>").openPopup();
         var circle = L.circle([4.667709, -74.054040], {
            color: 'red',
            fillColor: '#f03',
            fillOpacity: 0.5,
            radius: 50
        }).addTo(mymap);

        var popup6 = L.popup()
        .setLatLng([4.666830, -74.053050])
        .setContent("Centro Comercial")
        .openOn(mymap);
         var marker = L.marker([4.666830, -74.053050]).addTo(mymap);
         marker.bindPopup("<b>Centro Comercial Andino</b><br>").openPopup();
         var circle = L.circle([4.666830, -74.053050], {
            color: 'blue',
            fillColor: 'blue',
            fillOpacity: 0.5,
            radius: 50
        }).addTo(mymap);
       
        var popup7 = L.popup()
        .setLatLng([4.657313, -74.058237])
        .setContent("Iglesia de Porciuncula")
        .openOn(mymap);
         var marker = L.marker([4.657313, -74.058237]).addTo(mymap);
         marker.bindPopup("<b>Iglesia de Porciuncula</b><br>").openPopup();
         var circle = L.circle([4.657313, -74.058237], {
            color: 'green',
            fillColor: 'green',
            fillOpacity: 0.5,
            radius: 50
        }).addTo(mymap);
       
        var popup8 = L.popup()
        .setLatLng([4.664857, -74.058191])
        .setContent("Centro Comercial")
        .openOn(mymap);
         var marker = L.marker([4.664857, -74.058191]).addTo(mymap);
         marker.bindPopup("<b>Centro Comercial Unilago</b><br>").openPopup();
         var circle = L.circle([4.664857, -74.058191], {
            color: 'yellow',
            fillColor: 'yellow',
            fillOpacity: 0.5,
            radius: 50
        }).addTo(mymap);
       
       


    





        }
        
        